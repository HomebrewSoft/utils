======================
Code/Description Model
======================
This module creates an abstract model to have records unique by Code and with a description field.
Also, enables the search for both fields.
