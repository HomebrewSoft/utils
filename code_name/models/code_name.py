from os.path import dirname, join, realpath
from statistics import mode

from odoo import api, fields, models
from odoo.osv import expression


class CodeName(models.AbstractModel):
    _name = "code_name"
    _description = "Code/Name"

    name = fields.Char(
        index=True,
    )
    code = fields.Char(
        required=True,
        index=True,
    )

    def name_get(self):
        res = []
        for record in self:
            name = f"[{record.code}] {record.name}" if record.name else record.code
            res.append((record.id, name))
        return res

    @api.model
    def _name_search(self, name, args=None, operator="ilike", limit=100, name_get_uid=None):
        args = args or []
        if operator == "ilike" and not (name or "").strip():
            domain = []
        else:
            domain = ["|", ("name", "ilike", name), ("code", "ilike", name)]
        record_ids = self._search(
            expression.AND([domain, args]), limit=limit, access_rights_uid=name_get_uid
        )
        return self.browse(record_ids).name_get()

    def _install_hook(self, csv_path, module):
        """Import CSV data as it is faster than xml and because we can't use
        noupdate anymore with csv
        Even with the faster CSVs, it would take +30 seconds to load it with
        the regular ORM methods, while here, it is under 3 seconds
        """
        model_name = self._name
        model_table = model_name.replace(".", "_")
        csv_file = open(csv_path, "rb")
        headers = csv_file.readline()  # Read the header, so we avoid copying it to the db
        headers = ", ".join(headers.decode("utf-8")[:-1].split("|"))
        cr = self._cr
        cr.copy_expert(
            f"""COPY {model_table} ({headers})
            FROM STDIN WITH DELIMITER '|'""",
            csv_file,
        )
        # Create xml_id, to allow make reference to this data
        cr.execute(
            f"""INSERT INTO ir_model_data
            (name, res_id, module, model, noupdate)
            SELECT concat('{model_table}_', code), id, '{module}', '{model_name}', 't'
            FROM {model_table}"""
        )

    def _uninstall_hook(self):
        """Remove all data from this model"""
        cr = self._cr
        model_name = self._name
        model_table = model_name.replace(".", "_")
        cr.execute(f"DELETE FROM {model_table};")
        cr.execute(f"DELETE FROM ir_model_data WHERE model='{model_name}';")
